Roles to manage openshift objects
=================================

See each role README for instructions

Installation
============

Due to sub roles including each others, this role need to be named openshift, or it
will break.
